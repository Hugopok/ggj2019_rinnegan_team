﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MoreMountains;
using MoreMountains.CorgiEngine;

public class GGameManager: MonoBehaviour
{
    private static GGameManager instance = null;

    public static GGameManager Instance { get { return instance; } }

    public GameState GameState
    {
        get { return this._gameState; }

        set
        {
            this._gameState = value;

            if (this.OnGameStateChanged != null)
                this.OnGameStateChanged(this._gameState);
        }
    }

    public MultiplayerLevelManager MultiplayerLevelManager;

    public float MatchTimer
    {
        get { return this._matchTimer; }
    }

    public Action<GameState> OnGameStateChanged;

    public Action OnUpdate;

    public Action OnSecondElapsed;

    [SerializeField]
    private float _matchTimer;

    private GameState _gameState;

    public Animator startUIAnim;

    private void Awake()
    {

        if (instance == null)
            //if not, set it to this.
            instance = this;
        //If instance already exists:
        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        if (this.MultiplayerLevelManager == null)
            this.MultiplayerLevelManager = GameObject.FindObjectOfType<MultiplayerLevelManager>();

        this.FreezeAllPlayers();

        this.OnGameStateChanged += this.ResetValues;
        this.OnGameStateChanged += this.CheckCountdown;
        this.OnGameStateChanged += this.SetPlayersState;


        SpawnRandomKey.SpawnKey(this.SpawnPlayerInRoom());
        
    }

    public void InitGame()
    {        
        SoundManager.instance.PlayMusic("mainloop");

        this.GameState = GameState.Play;

    }

    public List<GameObject> pointStart = new List<GameObject>();

    private List<int> SpawnPlayerInRoom()
    {
        var ret = new List<int>();
        var roomSpawn = FindObjectsOfType<Room>();

        foreach (var player in this.pointStart)
        {
            var index = UnityEngine.Random.Range(0, roomSpawn.Length - 1);

            ret.Add(roomSpawn[index].floorLevel);

            player.transform.position = roomSpawn[index].transform.position;
        }
        return ret;
    }

    private void Update()
    {
        

        if (Input.GetKeyDown(KeyCode.Escape))
            this.GameState = GameState.Pause;

        if (this._gameState == GameState.Play)
        {
            if (this.OnUpdate != null)
                this.OnUpdate();
        }
        else if (this._gameState == GameState.Pause)
        {
            // TODO: Fai qualcosa!!
        }
    }

    public void FreezeAllPlayers()
    {
        if (this.MultiplayerLevelManager != null && this.MultiplayerLevelManager.Players != null)
        {
            foreach (var player in this.MultiplayerLevelManager.Players)
            {
                if (player == null)
                    continue;

                player.GetComponent<CharacterHorizontalMovement>().AbilityPermitted = false;
                player.GetComponent<CharacterJump>().AbilityPermitted = false;
                player.Freeze();
            }
        }
    }

    public void UnfreezeAllPlayers()
    {
        if (this.MultiplayerLevelManager != null && this.MultiplayerLevelManager.Players != null)
        {

            foreach (var player in this.MultiplayerLevelManager.Players)
            {
                if (player == null)
                    continue;
                player.GetComponent<CharacterHorizontalMovement>().AbilityPermitted = true;
                player.GetComponent<CharacterJump>().AbilityPermitted = true;
                
                player.UnFreeze();
            }
        }
    }

    private void SetPlayersState(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.Play:
                this.UnfreezeAllPlayers();
                break;
            case GameState.Pause:
                this.FreezeAllPlayers();
                break;
            default:
                break;
        }
    }

    public void EndGame()
    {

        StartCoroutine(_EndGame());

    }

    private IEnumerator _EndGame()
    {
        SoundManager.instance.PlaySingle("scureggia1");
        yield return new WaitForSeconds(1f);

        this.FreezeAllPlayers();

        SoundManager.instance.PlaySingle("scureggia2");
        yield return new WaitForSeconds(1f);

        SoundManager.instance.PlaySingle("scureggia1");
        yield return new WaitForSeconds(0.5f);
        SoundManager.instance.PlaySingle("scureggia2");
        yield return new WaitForSeconds(1f);

        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");

        Destroy(gameObject);


    }

    public void RestartMatch()
    {
        // TODO: Vedere se fare un brutale reload scene, oppure settare a mano il reset scene

        this.GameState = GameState.Restart;
    }

    private void CheckCountdown(GameState gameState)
    {
        switch (gameState)
        {
            case GameState.Play:
                this.StartCoroutine(this.Countdown(gameState));
                break;
            case GameState.Pause:
                break;
            case GameState.Restart:
                this.StopCoroutine(this.Countdown(gameState));
                break;
            default:
                break;
        }
    }

    private IEnumerator Countdown(GameState gameState)
    {
        while (this._gameState == GameState.Play)
        {
            //Debug.Log("Countdown: " + this._matchTimer);
            yield return new WaitForSeconds(1.0f);
            this._matchTimer++;
            if (OnSecondElapsed != null)
            {
                OnSecondElapsed();
            }
        }
    }

    private void ResetValues(GameState gameState)
    {
        if(gameState == GameState.Restart)
        {
            // TODO: Reset Values
            this._matchTimer = 0;

        }
    }
}
