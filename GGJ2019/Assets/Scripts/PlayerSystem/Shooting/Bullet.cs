﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MoreMountains.CorgiEngine;

public class Bullet : MonoBehaviour
{
    private int sign = 1;
    //private Animator anim;

    private bool bulletIsMoving = false;
    
    [SerializeField]
    private float bulletSpeed;

    public Action<Character> OnPlayerHitted; 

    // Use this for initialization
    public void StartBullet(bool goRight, Vector3 startPos, float speed)
    {
     //   anim = this.GetComponent<Animator>();

        if (goRight)
            sign = 1;
        else
            sign = -1;

        this.transform.position = startPos;
        this.bulletSpeed = speed;
        GGameManager.Instance.OnGameStateChanged += (GameState p) =>
        {
            if (p == GameState.Pause)
                this.bulletIsMoving = false;
            if (p == GameState.Play)
                this.bulletIsMoving = true;
        };
        this.gameObject.SetActive(true);
        bulletIsMoving = true;
    }

    private void OnDestroy()
    {
      
    }

    private void Update()
    {
        if(this.bulletIsMoving)
        this.GoStraight();
    }

    private void FixedUpdate()
    {
        if(this.bulletIsMoving)
        {
            
        }
    }

   // Collider2D hit;

    private void GoStraight()
    {
        if (bulletIsMoving == false)
            return;

        var hit = Physics2D.OverlapPoint(new Vector2(this.transform.position.x, this.transform.position.y));
        this.transform.Translate((sign * Vector3.right) * Time.deltaTime * bulletSpeed);

        if (hit)
        {
            if (hit.gameObject.layer == LayerMask.NameToLayer("Player")) //Colpito il giocatore
            {
                Debug.Log("HIT: PLAYER");

                hit.GetComponent<StunPlayer>().Stun();

                BulletHit();
                
                return;
            }
            
            if (hit.gameObject.layer == LayerMask.NameToLayer("Platforms"))  //Colpito un nemico Ostacolo
            {
                // Debug.Log("HIT: NOTHING");
                BulletHit();
                return;
            }
        }
    }

    private void BulletHit()
    {
      //  if (anim)
      //      anim.SetTrigger("isHit");
        this.DestroyBullet(0.1f);
        bulletIsMoving = false;
    }

    private void DestroyBullet(float time = 0.0f)
    {
        Destroy(this.gameObject, time);
    }

    private void OnBecameInvisible()
    {
        DestroyBullet();
    }
}



