﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public bool HasKey = false;
    public int Maxammo = 2;
    public float MaxTimeOfStun = 3;
    public int ShitLevel = 0;
    public int DoorLevel = 0;
    public static float KeyPickupTime = -1f;
    public static float KeyDropTime = -1f;
    public static float KeyPickupCooldown = 3f;
}
