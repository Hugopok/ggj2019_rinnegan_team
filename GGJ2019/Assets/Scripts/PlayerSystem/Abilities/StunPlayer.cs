﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class StunPlayer : CharacterAbility
{
    private bool isStunned;
    public float maxTimeOfStun;
    private float timeOfTheStun;

    private PlayerStats playerStats;
    private Character otherCharacter;
    private ShitSystem shitSystem;
    private bool shit;
    private CharacterJump characterJump;

    private GameObject stunSfx;

    protected override void Start()
    {
        base.Start();

        this.playerStats = this.GetComponent<PlayerStats>();
        this.maxTimeOfStun = this.playerStats.MaxTimeOfStun;
        this.shitSystem = this.GetComponent<ShitSystem>();
        this.characterJump = this.GetComponent<CharacterJump>();
        this.shitSystem.Completed += ShitSystem_Completed;
        GGameManager.Instance.OnSecondElapsed += this.ElsapedTime;
    }

    private void ShitSystem_Completed(bool obj)
    {
        if (!obj)
        {
            this.shit = true;
            // Debug.Log("Oh oh me so cacato sotto " + obj);
            if (this._character.PlayerID == "Player1")
                SoundManager.instance.PlaySingle("scureggia1");
            else
                SoundManager.instance.PlaySingle("scureggia2");

            this._character._animator.SetBool("Shit",true);
            stunSfx = Instantiate(Resources.Load<GameObject>("SFX_Stun"));
            stunSfx.transform.position = this.transform.position;
            this.Stun();
        }
    }

    public void Stun()
    {
        // Debug.Log("Ok vado freezo il character");

        // TODO: Set animation più audio

        this._characterBasicMovement.AbilityPermitted = false;
        this.characterJump.AbilityPermitted = false;
        
        this.isStunned = true;

        this.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionX;

        if (this.playerStats.HasKey)
        {
            this.DropKey();
        }
    }

    public GameObject glow;
    private void DropKey()
    {
        GameObject key = SpawnRandomKey.KeyReference;
        if (key == null)
        {
            Debug.Log("cannot find key");
            return;
        }

        Debug.Log("Dropped the key");
        PlayerStats.KeyDropTime = Time.time;
        this.playerStats.HasKey = false;
        key.transform.position = this.transform.position;
        key.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
        key.SetActive(true);
        key.GetComponent<Rigidbody2D>().AddForce(-this._character.transform.forward);
        glow.SetActive(false);
    }

    private void ElsapedTime()
    {
        if (!this.isStunned) // se non è stunnato non fa niente
            return;

        if (this.timeOfTheStun <= this.maxTimeOfStun)
            this.timeOfTheStun++;
        else
        {
            if (shit)
            {
                shit = false;
                this._character._animator.SetBool("Shit", false);
                Destroy(stunSfx.gameObject);
            }

            this.timeOfTheStun = 0;
            this.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            this._characterBasicMovement.AbilityPermitted = true;
            this.characterJump.AbilityPermitted = true;
        }

         //Debug.Log("Time of the stun " + this.timeOfTheStun);
    }
}
