﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class OpenDoor : CharacterAbility
{
    Transform leftBathroomDoor;
    Transform rightBathroomDoor;
    public float requiredDistance = 2f;

    protected override void Start()
    {
        base.Start();
        GGameManager.Instance.OnUpdate += UpdateCode;
        GameObject[] doors = GameObject.FindGameObjectsWithTag("Door");
        Debug.Log(string.Join(",", doors.Select(d=>d.name).ToArray()));
        leftBathroomDoor = doors.Where(d => d.name.ToLower().Contains("left")).First<GameObject>().transform;
        rightBathroomDoor = doors.Where(d => d.name.ToLower().Contains("right")).First<GameObject>().transform;
    }

    void UpdateCode()
    {
        float dist1 = Vector2.Distance(gameObject.transform.position, leftBathroomDoor.transform.position);
        float dist2 = Vector2.Distance(gameObject.transform.position, rightBathroomDoor.transform.position);
        if (gameObject.GetComponent<PlayerStats>().HasKey && (dist1 < requiredDistance || dist2 < requiredDistance) &&
            rightBathroomDoor.gameObject.activeSelf &&
            leftBathroomDoor.gameObject.activeSelf)
        {
            GameObject bathroomDoor;
            bathroomDoor = (dist1 < dist2 ? leftBathroomDoor : rightBathroomDoor).gameObject;
            SoundManager.instance.PlaySingle("apribagno");
            bathroomDoor.SetActive(false);

        }
    }
}
