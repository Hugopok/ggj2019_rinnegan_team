﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

[RequireComponent(typeof(StunPlayer))]
public class LaunchStuff : CharacterAbility
{
    public GameObject shoesPredfab;
    public Transform shoesLaunchPos;

    public GameObject leftAmmo, rightAmmo;

    private int maxAmmo = 2;
    private int ammo = 2;

    private StunPlayer stunPlayer;
    private PlayerStats playerStats;

    protected override void Start()
    {
        base.Start();
        this.playerStats = GetComponent<PlayerStats>();
        this.stunPlayer = this.GetComponent<StunPlayer>();
    
    }

    protected override void InitializeAnimatorParameters()
    {
        RegisterAnimatorParameter("LaunchShoes", AnimatorControllerParameterType.Trigger);
    }

   /* public override void UpdateAnimator()
    {
        
    }*/

    protected override void InternalHandleInput()
    {
        if (this._character.PlayerID != this._inputManager.PlayerID)
        {
            return;
        }

        if (GGameManager.Instance.GameState != GameState.Play)
            return;
 
        if (this._inputManager.RunButton.State.CurrentState == MoreMountains.Tools.MMInput.ButtonStates.ButtonDown)
        {
            if (this.ammo > 0)
            {
                if (this.shoesPredfab != null)
                {
                    var shoes = GameObject.Instantiate(this.shoesPredfab);

                    var bullet = shoes.GetComponent<Bullet>();

                    if (bullet != null)
                    {
                        SoundManager.instance.PlaySingle("ciavatta");

                        if (this._character.IsFacingRight)
                            bullet.StartBullet(this._character.IsFacingRight,new Vector3(this.transform.position.x + 0.65f,this.transform.position.y,this.transform.position.z),25);
                        else
                            bullet.StartBullet(this._character.IsFacingRight,new Vector3(this.transform.position.x - 0.65f, this.transform.position.y, this.transform.position.z), 25);

                        
                         MMAnimator.UpdateAnimatorTrigger(_animator, "LaunchShoes", _character._animatorParameters);

                        this.ammo--;

                    }
                    else
                    {
                        Debug.LogError("Non c'è nessuno script bullet al prefab");
                    }

                    if (ammo == 1)
                        rightAmmo.gameObject.SetActive(false);
                    if (ammo == 0)
                        leftAmmo.gameObject.SetActive(false);
                    
                }
                else
                {
                    Debug.LogError("Prefab is null");
                }
            }   
        }
    }

    public override void ProcessAbility()
    {
        base.ProcessAbility();
    }
}
