﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickupKey : CharacterAbility
{
    public Image gradient;

    private PlayerStats _stats;

    protected override void Start()
    {
        base.Start();
        this._stats = gameObject.GetComponent<PlayerStats>();
        //GGameManager.Instance.OnUpdate += KeyCollisionChecker;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.ToLower() == "key") {
            if (Time.time <= PlayerStats.KeyDropTime + PlayerStats.KeyPickupCooldown)
            {
                Debug.Log("Key in cool down time:" + Time.time + ", dropTime: " + PlayerStats.KeyDropTime);
                return;
            }

            if (!collision.gameObject.activeSelf)
                return;

            Debug.Log("Presa la chiave");

            PlayerStats.KeyPickupTime = Time.time;

            collision.gameObject.SetActive(false);
            this._stats.HasKey = true;
            if(this.gradient != null)
                gradient.gameObject.SetActive(true);

            SoundManager.instance.PlaySingle("prendichiave");

        }
    }

    private void Update()
    {
        this.gradient.gameObject.SetActive(this._stats.HasKey);
    }
}
