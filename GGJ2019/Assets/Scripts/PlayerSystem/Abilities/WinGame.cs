using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinGame : MonoBehaviour
{

    public GameObject p1win;
    public GameObject p2win;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.ToLower().Contains("player"))
        {
            if (collision.name.ToLower().Contains("player1"))
                p1win.SetActive(true);
            else
                p2win.SetActive(true);
            GGameManager.Instance.EndGame();
        }
        Debug.Log(collision.gameObject.name + " entered the bathroom");
    }
}
