﻿using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShitSelf : CharacterAbility
{
    public Image ShitEventButton;
    public Image ShitEventBarMask;
    public Image ShitEventBar;
    public int shitSelfWithoutKeyProbability = 15;
    public int shitSelfWithKeyProbability = 40;
    public int shitSelfPollingRate = 5;

    protected override void Start()
    {
        base.Start();
        GGameManager.Instance.OnSecondElapsed += SecondElapsed;
        gameObject.GetComponent<ShitSystem>().Completed += ShitSelf_Completed;
        gameObject.GetComponent<ShitSystem>().KeyPress += ShitSelf_KeyPress;
        //StartCoroutine(scaleResetter());
    }

    private string getKeyFileName(KeyCode key)
    {
        switch (key)
        {
            case KeyCode.Return:
            case KeyCode.Space:
            default:
                return "x";
        }
    }

    int secondsElapsed = 0;
    private void SecondElapsed()
    {
        if (!gameObject.GetComponent<ShitSystem>().Running)
        {
            secondsElapsed++;
            if (secondsElapsed >= shitSelfPollingRate)
            {
                secondsElapsed = 0;
                int random = Random.Range(0, 100);
                if (random < (gameObject.GetComponent<PlayerStats>().HasKey ? 40 : 15))
                {
                    KeyCode key = gameObject.GetComponent<ShitSystem>().ChooseKey();
                    //ShitEventButton.sprite = Resources.Load<Sprite>("Icons/Xbox/" + getKeyFileName(key));
                    gameObject.GetComponent<ShitSystem>().BeginEvent();
                    ShitEventButton.gameObject.SetActive(true);
                    ShitEventBarMask.gameObject.SetActive(true);
                }
            }
        }
    }

    private void ShitSelf_KeyPress(KeyCode obj)
    {
        //ShitEventButton.transform.localScale = new Vector2(1.5f, 1.5f);4
        // Debug.Log("Keypress event received");
        ShitEventBar.fillAmount += 1.0f / gameObject.GetComponent<ShitSystem>().strokesRequired;
        ShitEventBar.fillAmount = Mathf.Clamp(ShitEventBar.fillAmount, 0.01f, 1);
    }

    private void ShitSelf_Completed(bool survived)
    {
        ShitEventButton.gameObject.SetActive(false);
        ShitEventBarMask.gameObject.SetActive(false);
        ShitEventBar.fillAmount = 0.01f;
    }

    //IEnumerator scaleResetter ()
    //{
    //    yield return new WaitForEndOfFrame();
    //    if (ShitEventButton.transform.localScale.x >= 1 &&
    //        ShitEventButton.transform.localScale.y > 1)
    //    {
    //        ShitEventButton.transform.localScale -= new Vector3(.05f, .05f);
    //    }
    //}
}