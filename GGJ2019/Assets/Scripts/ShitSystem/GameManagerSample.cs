﻿using System;
using UnityEngine;

public class GameManagerSample : MonoBehaviour
{

    public GameObject player1;
    ShitSystem plr1ss;

    private void Start()
    {
        plr1ss = player1.GetComponent<ShitSystem>();
        plr1ss.ShatHimself += PlayerShitsHimself;
    }

    private void PlayerShitsHimself(GameObject playerRef, int timesClicked)
    {
        Debug.Log(playerRef.name + " shat himself with clicks: " + timesClicked.ToString());
    }

    void Update()
    {
        if (!plr1ss.Running)
        {
            plr1ss.BeginEvent();
        }
    }
}
