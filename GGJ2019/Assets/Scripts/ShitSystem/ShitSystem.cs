﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using MoreMountains.CorgiEngine;

public class ShitSystem : CharacterAbility
{

    #region fields & properties
    KeyCode chosenKey;

    private int strokes = 0;
    public int strokesRequired = 10;
    private float eventStart = 0;

    private bool running = false;
    public bool Running
    {
        get { return running; }
    }

    public event Action<GameObject, int> ShatHimself;
    public event Action<bool> Completed;
    public event Action<KeyCode> KeyPress;

    public static List<KeyCode> Keys = new List<KeyCode>()
    {
        KeyCode.Space
    };
    public static float DefaultTimeout = 3;
    #endregion

    protected override void Start()
    {
        base.Start();
        GGameManager.Instance.OnUpdate += UpdateCode;
    }

    public KeyCode ChooseKey()
    {
        chosenKey = Keys.OrderBy(x => Guid.NewGuid()).First();
        return chosenKey;
    }

    public void BeginEvent()
    {
        // Debug.Log(gameObject.name + " event started");
        strokes = 0;
        running = true;
        eventStart = GGameManager.Instance.MatchTimer;
        SoundManager.instance.PlaySingle("panza");
        //StartCoroutine(TimerCoroutine());
        //Debug.Log(chosenKey);
    }

    protected override void InitializeAnimatorParameters()
    {
        RegisterAnimatorParameter("Shit", AnimatorControllerParameterType.Trigger);
    }

    void UpdateCode()
    {
        if (!running)
            return;

        if (GGameManager.Instance.MatchTimer - eventStart > DefaultTimeout)
        {
            // timeout
            // Debug.Log("Timeout");
            running = false;
            if (ShatHimself != null)
            {
                //Debug.Log("Me so cacato sotto!");
                ShatHimself(gameObject, strokes);
            }
            if (Completed != null) { 
                Completed(false);
            }
        }

        if (this._inputManager.DashButton.State.CurrentState == MoreMountains.Tools.MMInput.ButtonStates.ButtonDown) 
            //Input.GetKeyDown(chosenKey))
        {
            strokes++;
            KeyPress(chosenKey);
            if (strokes == strokesRequired)
            {
                running = false;
                Completed(true);
                //Debug.Log("Mi sono salvato!");
            }
        }
    }

    //IEnumerator TimerCoroutine()
    //{
    //    yield return new WaitForSeconds(DefaultTimeout);
    //    if (ShatHimself != null && strokes < strokesRequired)
    //    {
    //        running = false;
    //        ShatHimself(gameObject, strokes);
    //    }
    //}
}
