﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent( typeof( Button ) )]
public class UIButton : MonoBehaviour
{

    private Button button;

    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(_Click);
    }

    private void _Click()
    {
        SoundManager.instance.PlaySingle("bclick");
        ClickButton();
    }
    void Update()
    {
        
    }

    public virtual void ClickButton() { }
}
