﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public Button play;
    public Button credits;

    void Start()
    {
        SoundManager.instance.PlayMusic("menuloop");
        
    }


    void Update()
    {
        
    }

    public void OpenCreditsPanel()
    {
        
    }


    public void BackButton()
    {
        SoundManager.instance.PlaySingle("apribagno");

        SceneManager.LoadScene("MainMenu");
    }

    public void OnPlayButton()
    {
        SoundManager.instance.PlaySingle("scureggia1");
    }

    public void OnCreditsButton()
    {
        SoundManager.instance.PlaySingle("scureggia2");

        SceneManager.LoadScene("Credits");
    }

    public void OnQuitButton()
    {
        SoundManager.instance.PlaySingle("salto");
    }
}
