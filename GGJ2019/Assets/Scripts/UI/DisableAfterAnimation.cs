﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableAfterAnimation : MonoBehaviour
{
    public void Disable()
    {
        this.gameObject.SetActive(false);
    }

    public void InitMatch()
    {
        GGameManager.Instance.InitGame();
    }

}
