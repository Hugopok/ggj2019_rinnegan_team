﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class SpawnRandomKey 
{

    public static GameObject KeyReference;
    public static void SpawnKey(List<int> placeWherePlayerSpawn)
    {
        //foreach (var p in placeWherePlayerSpawn)
        //    Debug.Log(" P SPAWN " + p);

        var allRooms = GameObject.FindObjectsOfType<Room>();

        var filteredRooms = new List<Room>();

        foreach(Room r in allRooms)
        {
            // Debug.Log(r.floorLevel);
            if (!placeWherePlayerSpawn.Contains(r.floorLevel))
                filteredRooms.Add(r);
        }

        var key = GameObject.Instantiate(Resources.Load<GameObject>("Key"));
        key.transform.position = filteredRooms[Random.Range(0, filteredRooms.Count - 1)].transform.position;
        KeyReference = key;
    }


}
