﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayButton : UIButton
{
    public override void ClickButton()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Game");
    }
}
